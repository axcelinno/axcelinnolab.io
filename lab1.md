## In this lab we will deploy a container to Azure Kubernetes Service (AKS) and demonstrate how to autoscale a container.

---
### Log into Azure and setup your environment

You have been provided a username and password for Azure. Navigate to the Azure [portal](portal.azure.com) and login.

After you have logged in, open the Azure Cloud Shell. The Azure Cloud Shell icon is located on the blue bar at the top of the page
and looks like a command prompt. The shell will open and ask you to select Bash or Powershell, select Bash. It will also ask you to add storage, -- select yes. Once everything is complete run the following commands:

```console
az account set -s 'AXC Workshops'

```

```console
az aks get-credentials --resource-group workshops --name dfwAKSCluster
```

Validate you are able to access the cluster by running the following command and verifying a list of pods is returned

```console
kubectl get nodes
```

---
### Deploying and scaling containers

#### Create a namespace
First, we don't want to step on each other so we will create namespaces. Create a file named namespace-USERNAME.json. Copy the following contents and update the NAME to your username:

```json
{
  "apiVersion": "v1",
  "kind": "Namespace",
  "metadata": {
    "name": "NAME",
    "labels": {
      "name": "NAME"
    }
  }
}
```

Now run the following command:

```console
kubectl create -f namespace-USERNAME.json
```

Now let's check for your namespace:

```console
kubectl get namespaces
```

#### Deploy a database
Now we are going to deploy a database, a ReST API, and a UI (it's not pretty, but it works.

Deploy the database by running the following command. Note: you must deploy the database in your namespace! Prior to running the command, update the command to contain your namespace. 

```console
helm install stable/mongodb --name orders-mongo-NAME --set mongodbUsername=orders-user,mongodbPassword=orders-password,mongodbDatabase=akschallenge --namespace NAME
```

#### Deploy an API
Now let's deploy the API. You need to deploy the Order Capture API (azch/captureorder). This requires an external endpoint, exposing the API on port 80 and writes to MongoDB. Save the follwing yaml as captureorder-deployment.yaml

Note: you must deploy the API in your namespace! Prior to running the command, update the file to contain your namespace. 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: captureorder
  namespace: NAME
spec:
  selector:
      matchLabels:
        app: captureorder
  replicas: 2
  template:
      metadata:
        labels:
            app: captureorder
      spec:
        containers:
        - name: captureorder
          image: azch/captureorder
          imagePullPolicy: Always
          readinessProbe:
            httpGet:
              port: 8080
              path: /healthz
          livenessProbe:
            httpGet:
              port: 8080
              path: /healthz
          resources:
            requests:
              memory: "128Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "500m"
          env:
          - name: MONGOHOST
            value: "orders-mongo-NAME-mongodb.NAME.svc.cluster.local"
          - name: MONGOUSER
            value: "orders-user"
          - name: MONGOPASSWORD
            value: "orders-password"
          ports:
          - containerPort: 8080
```

Deploy the API by running the following command:

```console
kubectl apply -f captureorder-deployment.yaml
```

Verify the pods are running. Make sure you add your namespace! 

```console
kubectl get pods -l app=captureorder --namespace NAME
```


#### Deploy a service
We have deployed the API, but now we need to be able to access it, so let's create a service. Save the follwing yaml as captureorder-service.yaml

Note: you must deploy the Service in your namespace! Prior to running the command, update the file to contain your namespace. 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: captureorder
  namespace: NAME
spec:
  selector:
    app: captureorder
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080
  type: LoadBalancer
```

```console
kubectl apply -f captureorder-service.yaml
```

Verify the service is running and retrieve the external IP. This may take a moment while as the Azure Load Balancer is assigning an IP address. Make sure you add your namespace! 

```console
kubectl get service captureorder -o jsonpath="{.status.loadBalancer.ingress[*].ip}" --namespace NAME
```

#### Validate Functionality
Now let's check to make sure the order API is writing to the database. Run the following command -- update the 'Your Service Public LoadBalancer IP' with value from the step before:

```console
curl -d '{"EmailAddress": "email@domain.com", "Product": "prod-1", "Total": 100}' -H "Content-Type: application/json" -X POST http://[Your Service Public LoadBalancer IP]/v1/order
```
If everything is working as expected you should receive a response like so:

``` json
{
    "orderId": "5beaa09a055ed200016e582f"
}
```

#### Deploy the UI
Now that we have the database and the API deployed, let's deploy the UI. You need to deploy the Frontend (azch/frontend). This requires an external endpoint, exposing the website on port 80 and needs to write to connect to the Order Capture API public IP.

We have deployed the API, but now we need to be able to access it, so let's create a service. Save the follwing yaml as frontend-deployment.yaml

Note: you must configure the deployment for your namespace! Prior to running the command, update the file to contain your namespace along with the Puplic IP of the API.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
  namespace: NAME
spec:
  selector:
      matchLabels:
        app: frontend
  replicas: 1
  template:
      metadata:
        labels:
            app: frontend
      spec:
        containers:
        - name: frontend
          image: azch/frontend
          imagePullPolicy: Always
          readinessProbe:
            httpGet:
              port: 8080
              path: /
          livenessProbe:
            httpGet:
              port: 8080
              path: /
          resources:
            requests:
              memory: "128Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "500m"
          env:
          - name: CAPTUREORDERSERVICEIP
            value: "<public IP of order capture service>"
          ports:
          - containerPort: 8080
```

Deploy using the following command
```console
kubectl apply -f frontend-deployment.yaml
```

Verify the pods are running. Make sure you add your namespace! 

```console
kubectl get pods -l app=frontend --namespace NAME
```


#### Autoscaling the API
The next step is to scale the API that was deployed. Save the YAML below as captureorder-hpa.yaml

Note: You must configure the autoscaler for your namespace! Prior to running the command, update the file to contain your namespace.

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: captureorder
  namespace: NAME
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: captureorder
  minReplicas: 4
  maxReplicas: 10
  targetCPUUtilizationPercentage: 50
```

Deploy using the following command
```console
kubectl apply -f captureorder-hpa.yaml
```

Now it's time to load the increase the traffic and watch the scaling take place automatically. Run the following command to launch a container that will send requests to your API.

Note: You will need to update the name to match your namespace along with the Public IP of the API.

```console
az container create -g workshops -n NAME --image azch/loadtest --restart-policy Never -e SERVICE_IP=<public ip of order capture service>
```
Now run this following command to see how many instances, or replicas, are created during the test;

```console
kubectl get pods -l app=captureorder --namespace NAME
```

#### Cleanup
Before moving to the next lab, let's clean up what we've done to ensure we have enough resources. Run the followinng commands:

```console
az container delete -g workshops -n NAME
```

```console
kubectl delete pod captureorder --namespace NAME
```

```console
kubectl delete pod frontend --namespace NAME
```
```console
kubectl delete pod captureorder --namespace NAME
```
---
Congrats! Now it's time to build a CI/CD process. 
