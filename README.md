Welcome to the Building a Secure, Scalable, and Flexible CI/CD Process workshop!

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Lab 1](#lab-1)
- [Lab 2](#lab-2)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Lab 1

In this lab we will deploy a container to Azure Kubernetes Service (AKS) and demonstrate
how to autoscale a container.

We will deploy a number of containers to understand how we can easily define an environment and dynically scale. 

## Lab 2

In this lab we will build pipelines in CloudBees Jenkins to undersatnd how our CI/CD process can scale.
