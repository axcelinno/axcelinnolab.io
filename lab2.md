## In this lab we will use CloudBees Jenkins to provide CI/CD capabilities at scale

---

### Log into CloudBees

The usename and password you were provided to log into Azure is used for CloudBees as well. Navigate to [CloudBees Jenkins](http://workshops.axcelinno.io/cjoc) and login.

While not covered in this lab, CloudBees has been setup to use Azure Active Directory to provide access. While your user has full access, this is not recommended. 


Now we will work togther to create a pipeline and leverage CloudBees Jenkins at scale